package com.cerence.OssLicenseAutomation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OssLicenseAutomationApplication {

	public static void main(String[] args) {
		SpringApplication.run(OssLicenseAutomationApplication.class, args);
	}

}
